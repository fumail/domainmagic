# -*- coding: utf-8 -*-
import unittest
import sys
sys.path.insert(0,'../src')
import domainmagic
from domainmagic.extractor import URIExtractor, fqdn_from_uri, EX_HACKS_PROCURL, EX_HACKS_LAZYHOSTNAME, EX_HACKS_IDNA, redirect_from_url

class Extractor(unittest.TestCase):
    def setUp(self):
        self.candidate=URIExtractor()
        self.candidate.skiplist=['skipme.com']
    
    def tearDown(self):
        pass
    
    
    def test_simple_text(self):
        txt="""hello http://bla.com please click on <a href="www.co.uk">slashdot.org/?a=c&f=m</a> www.skipme.com www.skipmenot.com/ x.co/4to2S http://allinsurancematters.net/lurchwont/ muahahaha x.org"""
        
        uris=self.candidate.extracturis(txt)
        self.assertTrue('http://bla.com' in uris, 'missing http://bla.com from %s, got only %s'%(txt,uris))
        self.assertTrue('www.co.uk' in uris)
        self.assertTrue('slashdot.org/?a=c&f=m' in uris)
        
        self.assertTrue('www.skipmenot.com/' in uris)
        #print " ".join(uris)
        self.assertTrue("skipme.com" not in " ".join(uris))
        
        self.assertTrue("http://allinsurancematters.net/lurchwont/" in uris)
        self.assertTrue("x.org" in uris,'rule at the end not found')
        self.assertTrue('x.co/4to2S','x.co short uri not found')

    def test_skipendwithdot(self):
        """Test if uri ending with dot is skipped if skipentry is present without dot"""
        txt = """hello www.skipme.com. WWW.SKIPME.COM. www.skipmenot.com/ """

        uris = self.candidate.extracturis(txt)
        #print " ".join(uris)
        self.assertTrue("skipme.com" not in " ".join((u.lower() for u in uris)))

    def test_skipendwithdot_CAPITAL(self):
        """Test if uri ending with dot is skipped if skipentry is present without dot"""
        txt = """hello M.ST. """

        candidate = URIExtractor()
        candidate.skiplist = ['m.st']

        uris = candidate.extracturis(txt)
        print(" ".join(uris))
        self.assertTrue("m.st" not in " ".join((u.lower() for u in uris)))

    def test_dotquad(self):
        txt="""click on 1.2.3.4 or http://62.2.17.61/ or https://8.8.8.8/bla.com """
        
        uris=self.candidate.extracturis(txt)
        self.assertTrue('1.2.3.4' in uris)
        self.assertTrue('http://62.2.17.61/' in uris)
        self.assertTrue('https://8.8.8.8/bla.com' in uris)
        
    def test_ipv6(self):
        txt="""click on http://[1337:1558:100b:1337:21b:21ff:fe9d:4e4a]/blah """
        
        uris=self.candidate.extracturis(txt)
        self.assertTrue('http://[1337:1558:100b:1337:21b:21ff:fe9d:4e4a]/blah' in uris,'ipv6 uri not extracted, got : %s'%uris)

        
    def test_uppercase(self):
        txt="""hello http://BLa.com please click"""
        uris=self.candidate.extracturis(txt)
        self.assertTrue('http://bla.com' not in uris,'uris should not be lowercased')
        self.assertTrue('http://BLa.com' in uris,'uri with uppercase not found')
        
    def test_url_without_file(self):
        txt="""lol http://roasty.familyhealingassist.ru?coil&commission blubb"""
        uris=self.candidate.extracturis(txt)
        self.assertTrue('http://roasty.familyhealingassist.ru?coil&commission' in uris,'did not find uri, result was %s'%uris)
        
    def test_negative(self):
        txt=""" yolo-hq.com&n=R3QY1V&c=0VZ1ND 1.2.3.4.5 1.2.3 2fwww.mktcompany.com.br%2forigem%2femail """
        uris=self.candidate.extracturis(txt)
        self.assertTrue(len(uris)==0,"Invalid uris should not have been extracted: %s"%uris)
        
    def test_usernamepw(self):
        txt=""" ftp://yolo:pw!_!@bla.com/blubb/bloing/baz.zip ftp://yolo@bla.com/blubb/bloing/baz.zip """
        uris=self.candidate.extracturis(txt)
        print(f"uris: {uris}")
        self.assertTrue('ftp://yolo:pw!_!@bla.com/blubb/bloing/baz.zip' in uris,'did not find uri with username and pw. result was %s'%uris)
        self.assertTrue('ftp://yolo@bla.com/blubb/bloing/baz.zip' in uris,'did not find uri with username. result was %s'%uris)
    
    
    def test_url_with_at(self):
        txt="""hello http://www.recswangy.com/news.php?email=kev-becca@wedding-bells.org.uk&clt=EH please click"""
        uris=self.candidate.extracturis(txt)
        self.assertTrue('http://www.recswangy.com/news.php?email=kev-becca@wedding-bells.org.uk&clt=EH' in uris,'uri with @ character not found')
        
    def test_ending_qmark(self):
        txt="""aaa http://hoostie.com/rescatenews/files/images/dw_logo.png?  bbb"""
        uris=self.candidate.extracturis(txt)
        self.assertTrue('http://hoostie.com/rescatenews/files/images/dw_logo.png?' in uris,'uri with ending ? not found')

    def test_url_with_bracket(self):
        txt = """hello http://phohoanglong.com/]Spyware please click"""
        uris = self.candidate.extracturis(txt)
        self.assertTrue('http://phohoanglong.com/]Spyware' in uris,'uri with ] character in path not found')

    def test_url_with_tilde(self):
        txt="""http://vanwinkle.de/NEW.IMPORTANT-NATWEST~BANKLINE-FORM/new_bankline.html please click"""
        uris=self.candidate.extracturis(txt)
        self.assertTrue('http://vanwinkle.de/NEW.IMPORTANT-NATWEST~BANKLINE-FORM/new_bankline.html' in uris,'uri with ~ character in path not found')


    def test_url_after_parentheses(self):
        txt=")http://vhyue.com/gbn3q/jahy6?id=8071100&pass=EmxUo4ST&mid=498270380&m=detail"
        uris=self.candidate.extracturis(txt)
        self.assertTrue('http://vhyue.com/gbn3q/jahy6?id=8071100&pass=EmxUo4ST&mid=498270380&m=detail' in uris,'uri after closing parentheses not found')

    def test_url_with_port(self):
        txt=" http://www.ironchampusa.ru:8177/247emaillists/ "
        uris=self.candidate.extracturis(txt)
        self.assertTrue('http://www.ironchampusa.ru:8177/247emaillists/' in uris,'uri with port not found')


    def test_fqdn_from_uri(self):
        self.assertEqual(fqdn_from_uri('http://www.ironchampusa.ru:8177/247emaillists/') ,'www.ironchampusa.ru')

    def test_url_with_leading_crap(self):
        txt="   ��*http://f5399r5hxs.com/epPgyPk/yYluS3/LPjyRhr/SlqRhe/YeuVlrX/maSsBVk/BiRJU "
        uris=self.candidate.extracturis(txt)
        self.assertTrue('http://f5399r5hxs.com/epPgyPk/yYluS3/LPjyRhr/SlqRhe/YeuVlrX/maSsBVk/BiRJU' in uris,'uri with leading crap chars not found')
        
    def test_url_in_parentheses(self):
        txt = "(http://example.com/bla/blubb)"
        uris = self.candidate.extracturis(txt)
        self.assertTrue('http://example.com/bla/blubb' in uris, 'uri in parentheses not found')

    def test_url_in_angle_brackets(self):
        txt = "<http://example.com/bla/blubb>"
        uris = self.candidate.extracturis(txt)
        self.assertTrue('http://example.com/bla/blubb' in uris, 'uri in parentheses not found')

    def test_url_with_dash(self):
        txt = "invalid@domain-invalid.com <mailto:invalid@domain-invalid.com%20> <http://www.domain-invalid.com> www.domain-invalid.com"
        uris = self.candidate.extracturis(txt)
        print(uris)
        self.assertTrue('http://www.domain-invalid.com' in uris)
        self.assertTrue(len(uris) == 1)

    def test_url_commaseparated(self):
        """Include all parameters separated by ',' as sub-delimiter"""
        txt = 'href="http://a.aaaa.domain-invalid.com/r/?id=aaaadaad,ddadadd,dddaddda&p1=aaaaaa.aaaa.aaaa.aaaaaaa.aaa%2Faaaa%2Faaaaa.html%23aaa.aaaaaaaaaaa@aaaaaaaaaaaa.aa"'
        uris = self.candidate.extracturis(txt)
        print(uris)
        self.assertIn('http://a.aaaa.domain-invalid.com/r/?id=aaaadaad,ddadadd,dddaddda&p1=aaaaaa.aaaa.aaaa.aaaaaaa.aaa%2Faaaa%2Faaaaa.html%23aaa.aaaaaaaaaaa@aaaaaaaaaaaa.aa', uris)
        self.assertTrue(len(uris) == 1)

    def test_url_googlequery(self):
        """Extract queried link from google with use_hacks being True"""
        txt = 'https://www.google.com/url?q=https://aaaaaaaa.aaa.aa/aaaaaaaa&source=aaaaa&aaa=dddddddddddddddd&bbb=AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'
        uris = self.candidate.extracturis(txt, use_hacks=True)
        print(uris)
        self.assertEqual(
            sorted(
                ['https://aaaaaaaa.aaa.aa/aaaaaaaa',
                 'https://www.google.com/url?q=https://aaaaaaaa.aaa.aa/aaaaaaaa&source=aaaaa&aaa=dddddddddddddddd&bbb=AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'
                 ]
            ), sorted(uris))

    def test_url_in_span(self):
        """url in span initially not working due to closing '>'"""
        txt = "<span>http://www.aaa.example.com</span>"
        uris = self.candidate.extracturis(txt)
        print(uris)
        self.assertEqual(['http://www.aaa.example.com'], uris)

    def test_url_in_span_withparam(self):
        """url in span initially not working due to closing '>'"""
        txt = "<span>http://www.aaa.example.com?p=aaaa</span>"
        uris = self.candidate.extracturis(txt)
        print(uris)
        self.assertEqual(['http://www.aaa.example.com?p=aaaa'], uris)

    def test_url_squarebrackets_domain(self):
        """Domain enclosed in square brackets should be extracted"""
        txt = "[http://domain-invalid.com]"
        uris = self.candidate.extracturis(txt)
        self.assertTrue('http://domain-invalid.com' in uris)

    def test_url_squarebrackets_path(self):
        """Domain and path enclosed in square brackets should be extracted"""
        txt = "[http://www.domain-invalid.com/bla]"
        uris = self.candidate.extracturis(txt)
        self.assertTrue('http://www.domain-invalid.com/bla' in uris)

    def test_url_squarebrackets_path2(self):
        """According to RFC no square brackets allowed in path, still we want to extract them"""
        txt = "[http://www.domain-invalid.com/bla[]/bla]"
        uris = self.candidate.extracturis(txt)
        self.assertTrue('http://www.domain-invalid.com/bla[]/bla' in uris)

    def test_url_squarebrackets_path_param(self):
        """Domain and path enclosed in square brackets should be extracted"""
        txt = "[http://www.domain-invalid.com/bla?id=aaaadaad,ddadadd&dummy[BLA]=100]"
        uris = self.candidate.extracturis(txt)
        self.assertTrue("http://www.domain-invalid.com/bla?id=aaaadaad,ddadadd&dummy[BLA]=100" in uris)

    def test_url_squarebrackets_pathsquare_param(self):
        """URI with path with [] enclosed in square brackets should be extracted"""
        txt = "[http://www.domain-invalid.com/bla[abc]?id=aaaadaad,ddadadd&dummy[BLA]=100]"
        uris = self.candidate.extracturis(txt)
        self.assertTrue("http://www.domain-invalid.com/bla[abc]?id=aaaadaad,ddadadd&dummy[BLA]=100" in uris)

    def test_username_hexip(self):
        """URI with username looking like domain, hostname as hex ip"""
        # Host is: 0xc0.0xa8.0x1.0x1 -> 192.168.1.1
        # username is: www.domain-invalid.com
        txt = "http://www.domain-invalid.com@0xc0.0xa8.0x01.0x01/bla"
        uris = self.candidate.extracturis(txt, use_hacks=True)
        print(f"uris: {uris}")
        self.assertIn("http://www.domain-invalid.com@0xc0.0xa8.0x01.0x01/bla", uris)

    def test_colon_no_protocol(self):
        """Allow a ':' in front of a uri if not in protocol"""
        txt = "confuse:www.domain-invalid.com"
        uris = self.candidate.extracturis(txt, use_hacks=True)
        print(f"uris: {uris}")
        self.assertIn("www.domain-invalid.com", uris)

    def test_parenthesis(self):
        """Allow uri enclosed in parentesis"""
        txt = "(www.domain-invalid.com)"
        uris = self.candidate.extracturis(txt, use_hacks=True)
        print(f"uris: {uris}")
        self.assertIn("www.domain-invalid.com", uris)

    def test_uri_in_params_unencoded(self):
        """unencoded -> find both"""
        txt = "https://bla.example.com/a/aa?data=adadaaaaadaaaaaaaadaa%2Baaaaaaaaaaaaaaaaaaaaaaadaaadaaaaaaaaa%2Baaaaadaaadaaaaaadadadaaaa%3D%3D!-!dadaaad!-!https://domain-invalid.com/aaaaaaa/adaaaaaaaaaaaaaaadaaaadaaa=="

        uris = self.candidate.extracturis(txt, use_hacks=0)
        uris = self.candidate.extracturis(txt, use_hacks=1)
        self.assertIn('https://domain-invalid.com/aaaaaaa/adaaaaaaaaaaaaaaadaaaadaaa==', uris)
        self.assertIn(txt, uris)

    def test_uri_in_params_encoded(self):
        """uri encoded -> find only main """

        txt = "https://bla.example.com/a/aa?data=adadaaaaadaaaaaaaadaa%2Baaaaaaaaaaaaaaaaaaaaaaadaaadaaaaaaaaa%2Baaaaadaaadaaaaaadadadaaaa%3D%3D%21-%21dadaaad%21-%21https%3A%2F%2Fdomain-invalid.com%2Faaaaaaa%2Fadaaaaaaaaaaaaaaadaaaadaaa=="

        uris = self.candidate.extracturis(txt, use_hacks=1)
        self.assertEqual([txt], uris)

    def test_uri_in_params_encoded_hacks2(self):
        """encoded and hacks1 -> find both"""

        txt = "https://bla.example.com/a/aa?data=adadaaaaadaaaaaaaadaa%2Baaaaaaaaaaaaaaaaaaaaaaadaaadaaaaaaaaa%2Baaaaadaaadaaaaaadadadaaaa%3D%3D%21-%21dadaaad%21-%21https%3A%2F%2Fdomain-invalid.com%2Faaaaaaa%2Fadaaaaaaaaaaaaaaadaaaadaaa=="
        uris = self.candidate.extracturis(txt, use_hacks=EX_HACKS_PROCURL | EX_HACKS_IDNA)
        self.assertIn('https://domain-invalid.com/aaaaaaa/adaaaaaaaaaaaaaaadaaaadaaa==', uris)
        self.assertIn(txt, uris)

    def test_hostname_endswith_dot(self):
        """Test extraction allows ending hostname with ."""
        txt = "http://aaa.dd.bla.example.org.///?aaa#.yyyyyyyyyyyyyyyyyyyyyyy="
        uris = self.candidate.extracturis(txt, use_hacks=EX_HACKS_PROCURL | EX_HACKS_IDNA)
        self.assertIn(txt, uris)

    def test_punycode(self):
        """make unicode 2 punycode before extracting link if hacks > 1"""
        txt = 'href="https://ẹxample.org/AAAAAAAAAAA?/aaaaaaaa/aaaaaaaaaaaaaa">'
        expected = 'https://xn--clgxample.org/AAAAAAAAAAA?/aaaaaaaa/aaaaaaaaaaaaaa'
        uris = self.candidate.extracturis(txt, use_hacks=EX_HACKS_PROCURL)
        self.assertNotIn(expected, uris)
        uris = self.candidate.extracturis(txt, use_hacks=EX_HACKS_PROCURL | EX_HACKS_IDNA)
        self.assertIn(expected, uris)

    def test_lazydomainname(self):
        """Use lazy hostname regex allowing "_" in hostname..."""
        txt = '<http://aa_a0_00.example.org/>'
        expected = 'http://aa_a0_00.example.org/'
        uris = self.candidate.extracturis(txt, use_hacks=EX_HACKS_PROCURL | EX_HACKS_IDNA)
        self.assertNotIn(expected, uris, "Enabling all other hacks but EX_HACKS_LAZYHOSTNAME "
                                         "should not lead to find uris with '_' in hostname...")
        uris = self.candidate.extracturis(txt, use_hacks=EX_HACKS_LAZYHOSTNAME)
        self.assertIn(expected, uris)
    
    def test_url_with_special_username(self):
        txt="https://facebook.com+login=secure+settings=private@g39ev8bw57xme-example.com/amz/Amazon/"
        uris=self.candidate.extracturis(txt)
        self.assertIn(txt, uris)
    
    def test_redirect_from_url(self):
        examples = {
            'https://urldefense.com/v3/__http:/www.example.com/foo/bar__;!!somehashdata$': 'http://www.example.com/foo/bar',
            'https://www.google.com/url?q=http://www.example.com/': 'http://www.example.com/',
            'viber://?text=http://www.example.com/': 'http://www.example.com/',
        }
        
        for example in examples:
            result = redirect_from_url(example)
            expect = examples[example]
            self.assertEqual(expect, result, f'Unexpected result for URI {example} expected {expect} got {result}')