# -*- coding: utf-8 -*-
import unittest
import SRS
import sys
sys.path.insert(0,'../src')
from domainmagic.mailaddr import is_srs, decode_srs, is_batv, strip_batv, email_normalise_ebl, split_mail, domain_from_mail, extract_salesforce


class MailaddrTest(unittest.TestCase):

    def setUp(self):
        self.srs = SRS.new(secret='domainmagicsecret', maxage=8, hashlength=8, separator='=', alwaysrewrite=True)
    
    def tearDown(self):
        pass
    
    def test_is_srs(self):
        address = 'user@example.com'
        self.assertFalse(is_srs(address), 'wrongfully recognized non-srs address %s' % address)
        address = self.srs.forward(address, 'srs.example.com')
        self.assertTrue(is_srs(address), 'not recognized srs address %s' % address)
        address = self.srs.forward(address, 'srs.example.org')
        self.assertTrue(is_srs(address), 'not recognized srs address %s' % address)
        
        for addr in ['srs0=510b34ff05d14cac=uc=example.com=user@srs.example.org',
                    'srs0+510b34ff05d14cac=uc=example.com=user@srs.example.org',
                    'srs1=510b34ff05d14cac=uc=example.com=user@srs.example.org',
                    'srs1+510b34ff05d14cac=uc=example.com=user@srs.example.org']:
            self.assertTrue(is_srs(addr), 'not recognized srs address %s' % addr)
        
    def test_decode_srs(self):
        address = 'user@example.com'
        self.assertEqual(decode_srs(address), address, 'decoded non-srs address %s' % address)
        srs0address = self.srs.forward(address, 'srs.example.com')
        self.assertEqual(decode_srs(srs0address), address, 'failed to decode srs address %s' % address)
        srs1address = self.srs.forward(srs0address, 'srs.example.org')
        self.assertEqual(decode_srs(srs1address), address, 'failed to decode srs address %s' % address)
        
        for addr in ['srs0=510b34ff05d14cac=uc=example.com=user@srs.example.org',
                    'srs0+510b34ff05d14cac=uc=example.com=user@srs.example.org',]:
            self.assertEqual(decode_srs(addr), address, 'failed to decode srs address %s' % addr)
        
    def test_is_batv(self):
        batvaddr = 'prvs=1234=user@example.com'
        self.assertTrue(is_batv(batvaddr), 'not recognized batv address %s' % batvaddr)
        address = 'user@example.com'
        self.assertFalse(is_batv(address), 'recognized non-batv address %s' % address)
    
    def test_strip_batv(self):
        address = 'user@example.com'
        batvaddr = 'prvs=1234=user@example.com'
        self.assertEqual(strip_batv(batvaddr), address, 'failed to decode batv address %s' % address)
        self.assertEqual(strip_batv(address), address, 'decoded non-batv address %s' % address)
    
    def test_split_mail(self):
        localparts = ['foo', 'foo@bar', 'foo@bar.com', 'foo@bar@baz.com']
        domain = 'example.com'
        for localpart in localparts:
            address = '%s@%s' % (localpart, domain)
            lp, dom = split_mail(address)
            self.assertEqual(localpart, lp, 'invalid detection of localpart %s as %s' % (localpart, lp))
            self.assertEqual(domain, dom, 'invalid detection of domain %s as %s' % (domain, dom))
    
    def test_domain_from_mail(self):
        localparts = ['foo', 'foo@bar', 'foo@bar.com', 'foo@bar@baz.com']
        domain = 'example.com'
        for localpart in localparts:
            address = '%s@%s' % (localpart, domain)
            dom = domain_from_mail(address)
            self.assertEqual(domain, dom, 'invalid detection of domain %s as %s' % (domain, dom))
            
    def test_normalise_ebl(self):
        sets = [
            (None, None),
            ('', ''),
            ('asdf', 'asdf'),
            ('example@googlemail.com', 'example@gmail.com'),
            ('foo+bar@example.com', 'foo@example.com'),
            ('+foo+bar@example.com', '+foo@example.com'),
            ('+foo@example.com', '+foo@example.com'),
            ('e.x.a.m.p.l.e@gmail.com', 'example@gmail.com'),
            ('e.x.a.m.p.l.e@googlemail.com', 'example@gmail.com'),
            ('example-tag@yahoo.com', 'example@yahoo.com'),
            ('-example-tag@yahoo.com', '-example@yahoo.com'),
            ('-example@yahoo.com', '-example@yahoo.com'),
            ('x-example@yahoo.com', 'x-example@yahoo.com'),
            ('id=foo@example.com', 'foo@example.com'),
        ]
        
        for i, o in sets:
            self.assertEqual(email_normalise_ebl(i), o, 'failed to normalise %s' % i)
    
    def test_extract_salesforce(self):
        sets = [
            (None, None),
            ('', ''),
            ('asdf', 'asdf'),
            ('example@gmail.com', 'example@gmail.com'),
            ('user=example.com__0-randomdata@randomdata.bnc.salesforce.com', 'user@example.com'),
            ('user=example.com_0-randomdata@randomdata.bnc.salesforce.com', 'user=example.com_0-randomdata@randomdata.bnc.salesforce.com'),
            ('user-example.com__0-randomdata@randomdata.bnc.salesforce.com', 'user-example.com__0-randomdata@randomdata.bnc.salesforce.com'),
        ]
        for i, o in sets:
            self.assertEqual(extract_salesforce(i), o, 'failed to extract %s' % i)
            
        
        