# -*- coding: utf-8 -*-
import unittest
from unittest.mock import patch
import sys
sys.path.insert(0,'../src')
from domainmagic.rbl import *

class RBLProviderBaseTest(unittest.TestCase):
    def setUp(self):
        self.r = RBLProviderBase('rbl.example.com')
    
    def test_add_replycode(self):
        self.assertEqual(len(self.r.replycodes), 0)
        self.r.add_replycode(0)
        self.assertEqual(self.r.replycodes[0], 'rbl.example.com')
        self.r.add_replycode(1, 'rbl.example.org')
        self.assertEqual(self.r.replycodes[1], 'rbl.example.org')
        self.assertEqual(len(self.r.replycodes), 2)
        
    def test_add_filters(self):
        self.assertIsNone(self.r.filters)
        self.r.add_filters(['foobar'])
        self.assertEqual(self.r.filters, ['foobar'])
    
    def test_check_fnmatch(self):
        trueresults = [
            ('127.0.0.1', '127.0.?.1'),
            ('127.0.0.1', '127.0.*'),
        ]
        for dnsresult, code in trueresults:
            self.assertTrue(self.r._check_fnmatch(dnsresult, code))
        
        falseresults = [
            ('127.0.0.1', '127.0.0.1'),
            ('127.0.0.2', '127.0.?.1'),
            ('127.1.0.1', '127.0.*'),
        ]
        for dnsresult, code in falseresults:
            self.assertFalse(self.r._check_fnmatch(dnsresult, code))
        
    def test_listed_identifiers(self):
        listings = self.r._listed_identifiers('testinput', 'testtransform', '127.0.0.1')
        self.assertEqual(len(listings), 0)
        
        self.r.add_replycode(1)
        
        listings = self.r._listed_identifiers('testinput', 'testtransform', '127.0.0.2')
        self.assertEqual(len(listings), 0)
        
        listings = self.r._listed_identifiers('testinput', 'testtransform', '127.0.0.1')
        self.assertEqual(len(listings), 1)
        self.assertEqual(listings[0][0], 'rbl.example.com')
        self.assertEqual(listings[0][1], 'testinput is listed on rbl.example.com (rbl.example.com)')
        
    def test_make_description(self):
        self.assertEqual(self.r.make_description(), '${input} is listed on rbl.example.com (${identifier})')
        self.assertEqual(self.r.make_description(input='foobar'), 'foobar is listed on rbl.example.com (${identifier})')
        self.assertEqual(self.r.make_description(identifier='foobar'), '${input} is listed on rbl.example.com (foobar)')
        self.assertEqual(self.r.make_description(input='barfoo', identifier='foobar'), 'barfoo is listed on rbl.example.com (foobar)')
    
    def test_accept_input(self):
        acceptables = ['example.com', '127.0.0.1', 'abcdef1234567890', 'other-example.com', 'something-else']
        for item in acceptables:
            self.assertTrue(self.r.accept_input(item), f'inacceptable input {item}')
        inacceptables = ['_example.com', 'user@example.com', 'asdf!', 'a', 257*'a', '::1']
        for item in inacceptables:
            self.assertFalse(self.r.accept_input(item), f'acceptable input {item}')
    
    def test_transform_input(self):
        self.assertEqual(self.r.transform_input('foobar'), ['foobar'])
    
    def test_make_lookup(self):
        self.assertEqual(self.r.make_lookup('foobar'), 'foobar.rbl.example.com')
        self.assertEqual(self.r.make_lookup('foobar.'), 'foobar.rbl.example.com')
        self.assertEqual(self.r.make_lookup('1.2.3.4'), '1.2.3.4.rbl.example.com')
        
        
class BitmaskedRBLProviderTest(unittest.TestCase):
    def setUp(self):
        self.r = BitmaskedRBLProvider('rbl.example.com')
        
    def test_listed_identifiers(self):
        listings = self.r._listed_identifiers('testinput', 'testtransform', '127.0.0.1')
        self.assertEqual(len(listings), 0)
        
        self.r.add_replycode(1)
        
        listings = self.r._listed_identifiers('testinput', 'testtransform', '127.0.0.2')
        self.assertEqual(len(listings), 0)
        
        listings = self.r._listed_identifiers('testinput', 'testtransform', '127.0.0.1')
        self.assertEqual(len(listings), 1)
        self.assertEqual(listings[0][0], 'rbl.example.com')
        self.assertEqual(listings[0][1], 'testinput is listed on rbl.example.com (rbl.example.com)')
        
        listings = self.r._listed_identifiers('testinput', 'testtransform', '128.0.0.1')
        self.assertEqual(len(listings), 0)
        
        self.r.failsafe = False
        listings = self.r._listed_identifiers('testinput', 'testtransform', '128.0.0.1')
        self.assertEqual(len(listings), 1)


class ReverseIPLookupTest(unittest.TestCase):
    def setUp(self):
        self.r = ReverseIPLookup('rbl.example.com')
    
    def test_make_lookup(self):
        self.assertEqual(self.r.make_lookup('foobar'), 'foobar.rbl.example.com')
        self.assertEqual(self.r.make_lookup('1.2.3.4'), '4.3.2.1.rbl.example.com')
        self.assertEqual(self.r.make_lookup('::1'), '1.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.rbl.example.com')


class BitmaskedIPOnlyProviderTest(unittest.TestCase):
    def setUp(self):
        self.r = BitmaskedIPOnlyProvider('rbl.example.com')
    
    def test_accept_input(self):
        acceptables = ['127.0.0.1', '::1']
        for item in acceptables:
            self.assertTrue(self.r.accept_input(item), f'inacceptable input {item}')
        inacceptables = ['_example.com', 'user@example.com', 'asdf!', 'a', 257*'a', 'example.com', 'abcdef1234567890']
        for item in inacceptables:
            self.assertFalse(self.r.accept_input(item), f'acceptable input {item}')


class DummyResolver:
    def lookup(self, question, qtype):
        dnr = DNSLookupResult()
        dnr.question = question
        dnr.qtype = qtype
        dnr.ttl = 0
        dnr.rtype = None
        if qtype == 'NS':
            dnr.content =  'ns.example.com'
        elif qtype == 'A':
            dnr.content =  '127.53.53.53'
        elif qtype == 'SOA':
            dnr.content =  'ns.example.com. hostmaster.example.com. 20250101000 7200 3600 604800 3600'
        return [dnr]

class ResolverTestException(Exception):
    pass

class ExceptionResolver:
    def lookup(self, question, qtype):
        raise ResolverTestException

class FixedResultNSNameProviderTest(unittest.TestCase):
    def setUp(self):
        self.r = FixedResultNSNameProvider('rbl.example.com')
        
    def test_transform_input(self):
        self.r.resolver = DummyResolver()
        self.assertEqual(self.r.transform_input('foobar'), ['ns.example.com'])
        self.r.resolver = ExceptionResolver()
        self.assertEqual(self.r.transform_input('foobar'), [])
    
    def test_accept_input(self):
        acceptables = ['example.com', '_example.com',]
        for item in acceptables:
            self.assertTrue(self.r.accept_input(item), f'inacceptable input {item}')
        inacceptables = ['127.0.0.1', '::1', 'user@example.com', 'asdf!', 'a', 257*'a', 'abcdef1234567890']
        for item in inacceptables:
            self.assertFalse(self.r.accept_input(item), f'acceptable input {item}')
    
    
class FixedResultNSIPProviderTest(FixedResultNSNameProviderTest):
    def setUp(self):
        self.r = FixedResultNSIPProvider('rbl.example.com')
        
    def test_transform_input(self):
        self.r.resolver = DummyResolver()
        self.assertEqual(self.r.transform_input('foobar'), ['127.53.53.53'])
        self.r.resolver = ExceptionResolver()
        self.assertEqual(self.r.transform_input('foobar'), [])
        
    def test_make_lookup(self):
        self.assertEqual(self.r.make_lookup('1.2.3.4'), '4.3.2.1.rbl.example.com')
        self.assertEqual(self.r.make_lookup('::1'), '1.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.rbl.example.com')

    
class SOAEmailProviderTest(unittest.TestCase):
    def setUp(self):
        self.r = SOAEmailProvider('rbl.example.com')
    
    def test_transform_input(self):
        self.r.resolver = DummyResolver()
        self.assertEqual(self.r.transform_input('foobar'), ['hostmaster.example.com'])
        self.r.resolver = ExceptionResolver()
        self.assertEqual(self.r.transform_input('foobar'), [])
    
    
class EmailBLSimpleProviderTest(unittest.TestCase):
    def setUp(self):
        self.r = EmailBLSimpleProvider('rbl.example.com')
    
    def test_accept_input_plain(self):
        r = EmailBLSimpleProvider('rbl.example.com')
        acceptables = ['user@example.com', 'user@example.org']
        for item in acceptables:
            self.assertTrue(self.r.accept_input(item), f'inacceptable input {item}')
        inacceptables = ['127.0.0.1', '::1', 'example.com', '_example.com','asdf!', 'a', 257*'a', 'abcdef1234567890']
        for item in inacceptables:
            self.assertFalse(self.r.accept_input(item), f'acceptable input {item}')
    
    def test_accept_input_listed(self):
        def dummydomainlist(*args):
            return ['example.com']
        self.r._get_domainlist = dummydomainlist
        acceptables = ['user@example.com']
        for item in acceptables:
            self.assertTrue(self.r.accept_input(item), f'inacceptable input {item}')
        inacceptables = ['user@example.org', '127.0.0.1', '::1', 'example.com', '_example.com','asdf!', 'a', 257*'a', 'abcdef1234567890']
        for item in inacceptables:
            self.assertFalse(self.r.accept_input(item), f'acceptable input {item}')
    
    def test_get_format(self):
        hashtype, extractor, normaliser, encoder = self.r._get_format()
        self.assertEqual(hashtype, 'md5')
        self.assertEqual(normaliser, email_normalise_low)
        
        self.r.filters = 'sh:sha1:hex'.split(':')
        hashtype, extractor, normaliser, encoder = self.r._get_format()
        self.assertEqual(hashtype, 'sha1')
        self.assertEqual(normaliser, email_normalise_sh)
        
        self.r.filters = 'ebl:sha256:b64'.split(':')
        hashtype, extractor, normaliser, encoder = self.r._get_format()
        self.assertEqual(hashtype, 'sha256')
        self.assertEqual(normaliser, email_normalise_ebl)
        
        self.r.filters = 'foobar:foobar:foobar'.split(':')
        hashtype, extractor, normaliser, encoder = self.r._get_format()
        self.assertEqual(hashtype, 'md5')
        self.assertEqual(normaliser, email_normalise_low)
    
    def test_transform_input(self):
        self.assertEqual(self.r.transform_input('user@example.com'), ['b58996c504c5638798eb6b511e6f49af'])
        self.assertEqual(self.r.transform_input('prvs=0980cedfe=user@example.com'), ['3085c7322345b3df2bdb259231012606'])
        self.assertEqual(self.r.transform_input('srs0=510b34ff05d14cac=uc=example.com=user@srs.example.org'), ['50a4745e493cce5484ddc55bab4ec886'])
        self.r.filters = 'sh:sha1:hex'.split(':')
        self.assertEqual(self.r.transform_input('user@example.com'), ['63a710569261a24b3766275b7000ce8d7b32e2f7'])
        self.r.filters = 'sh:sha1:b64'.split(':')
        self.assertEqual(self.r.transform_input('user@example.com'), ['Y6cQVpJhoks3ZidbcADOjXsy4vc'])
        self.r.filters = 'sh:sha1:b32'.split(':')
        self.assertEqual(self.r.transform_input('user@example.com'), ['MOTRAVUSMGREWN3GE5NXAAGORV5TFYXX'])
        
        
class EmailBLMultiProviderTest(unittest.TestCase):
    def setUp(self):
        self.r = EmailBLMultiProvider('rbl.example.com')
    
    def test_transform_input(self):
        self.assertEqual(self.r.transform_input('user@example.com'), ['b58996c504c5638798eb6b511e6f49af'])
        self.assertEqual(set(self.r.transform_input('prvs=0980cedfe=user@example.com')), {'b58996c504c5638798eb6b511e6f49af', '3085c7322345b3df2bdb259231012606'})
        self.assertEqual(set(self.r.transform_input('srs0=510b34ff05d14cac=uc=example.com=user@srs.example.org')), {'b58996c504c5638798eb6b511e6f49af', '50a4745e493cce5484ddc55bab4ec886'})


class FixedResultDomainProviderTest(unittest.TestCase):
    def setUp(self):
        self.r = FixedResultDomainProvider('rbl.example.com')
    
    def test_accept_input(self):
        acceptables = ['example.com', 'abcdef1234567890']
        for item in acceptables:
            self.assertTrue(self.r.accept_input(item), f'inacceptable input {item}')
        inacceptables = ['127.0.0.1', '::1', 'user@example.com', 'asdf!', 'a', 257*'a', '_example.com']
        for item in inacceptables:
            self.assertFalse(self.r.accept_input(item), f'acceptable input {item}')
            