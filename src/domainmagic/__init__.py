# -*- coding: UTF-8 -*-
#   Copyright 2012-2022 Fumail Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
#


__version__ = "0.0.15"


def check_installation():
    """check dependencies , returns a list of problems"""
    problems = []

    from domainmagic.ip import PYGEOIP_AVAILABLE

    if not PYGEOIP_AVAILABLE:
        problems.append("pygeoip is not installed - geoip functions disabled")

    try:
        from dns import resolver
    except ImportError:
        problems.append("dnspython is not installed")

    return problems
